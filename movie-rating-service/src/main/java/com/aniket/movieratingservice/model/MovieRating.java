package com.aniket.movieratingservice.model;

import lombok.*;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieRating {

    int movieId;
    Double movieRating;

}
