package com.aniket.movieinfoservice.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieInfo {
int id ;
String name;
String description;
}
