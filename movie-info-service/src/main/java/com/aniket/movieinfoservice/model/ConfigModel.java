package com.aniket.movieinfoservice.model;

import lombok.*;

@Data
@AllArgsConstructor
public class ConfigModel {

    private String infoPropertyCommon;
    private String infoPropertyOne;
    private String infoPropertyTwo;
    private String myProfile;
    private String superCommonKey;


}
