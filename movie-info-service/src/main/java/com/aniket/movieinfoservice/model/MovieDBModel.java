package com.aniket.movieinfoservice.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieDBModel {
    private String id;
    private String original_title;
    private String overview;
}
