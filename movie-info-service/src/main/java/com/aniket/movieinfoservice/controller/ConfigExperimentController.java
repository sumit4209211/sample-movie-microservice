package com.aniket.movieinfoservice.controller;

import com.aniket.movieinfoservice.model.ConfigModel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope // This is mandatory to kick in Refresh
public class ConfigExperimentController {
    @Value("${info-property-common}")
    private String infoPropertyCommon;
    @Value("${info-property-one}")
    private String infoPropertyOne;
    @Value("${info-property-two}")
    private String infoPropertyTwo;
    @Value("${my-profile}")
    private String myProfile;
    @Value("${super-common-key}")
    private String superCommonKey;


    @GetMapping("/getInfoConfigs")
    public ConfigModel getConfigs() {

        return new ConfigModel(infoPropertyCommon, infoPropertyOne, infoPropertyTwo, myProfile, superCommonKey);
    }

}
