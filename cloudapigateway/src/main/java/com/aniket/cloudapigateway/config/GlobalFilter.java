package com.aniket.cloudapigateway.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class GlobalFilter extends AbstractGatewayFilterFactory<AbstractGatewayFilterFactory.NameConfig> {

    @Autowired
    private ServerProperties serverProperties;

    @Override
    public GatewayFilter apply(NameConfig config) {
      log.info("-----Calling VIA API GATEWAY ----- AT PORT {}",serverProperties.getPort());
        return (exchange , chain ) -> {
return chain.filter(exchange);
        };
    }
}
