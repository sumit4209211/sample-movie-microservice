package com.aniket.vanilajavaapicaller.controller;


import com.aniket.vanilajavaapicaller.feignClients.MovieCatalogClient;
import com.aniket.vanilajavaapicaller.model.CatalogItem;
import com.aniket.vanilajavaapicaller.model.CatalogResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@Slf4j
public class MainController {

    @Autowired
    private MovieCatalogClient feignProxy;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/getCatalog/feign/{userid}")
    public CatalogResponse getCatalogResponseFeign(@PathVariable String userid)
    {
        log.info("-----Calling via Feign Client------");
        CatalogResponse catalogResponse = feignProxy.getItems(userid);
        catalogResponse.setUserId(userid);
        return  catalogResponse;
    }

    @GetMapping("/getCatalog/resttemplate/{userid}")
    public CatalogResponse getCatalogResponseRestTemplate(@PathVariable String userid)
    {
        log.info("-----Calling via Rest Template Client------");
        CatalogResponse catalogResponse = restTemplate.getForObject("http://root-to-api-gateway/movie-catalog-service/catalog/"+userid,CatalogResponse.class);
        catalogResponse.setUserId(userid);
        return  catalogResponse;
    }
}
