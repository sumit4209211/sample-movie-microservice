package com.aniket.vanilajavaapicaller.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CatalogItem {
    int id;
    String movieName;
    String movieDescription;
    Double movieRating;
    
}
