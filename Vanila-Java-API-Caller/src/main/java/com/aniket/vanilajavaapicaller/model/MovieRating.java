package com.aniket.vanilajavaapicaller.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieRating {

    int movieId;
    Double movieRating;

}
