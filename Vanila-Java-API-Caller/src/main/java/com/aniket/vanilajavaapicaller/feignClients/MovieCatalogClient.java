package com.aniket.vanilajavaapicaller.feignClients;

import com.aniket.vanilajavaapicaller.model.CatalogResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//This is when U do not have a lb present or any Service discovery client attached to it
//@FeignClient(name = "root-to-api-gateway",url = "http://localhost:8765")
//This is when We are using a lb - where server instances are passed manually
@FeignClient(name = "root-to-api-gateway")
public interface MovieCatalogClient {

    @GetMapping("/movie-catalog-service/catalog/{userId}")
     CatalogResponse getItems(@PathVariable("userId") String id);
}
