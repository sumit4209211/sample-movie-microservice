package com.aniket.vanilajavaapicaller.config;

import org.springframework.cloud.client.DefaultServiceInstance;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier;
import reactor.core.publisher.Flux;

import java.util.Arrays;
import java.util.List;

public class SpringCloudLoadBalancerConfig  implements ServiceInstanceListSupplier {
    private final String serviceId;

    SpringCloudLoadBalancerConfig(String serviceId) {
        this.serviceId = serviceId;
    }

    @Override
    public String getServiceId() {
        return serviceId;
    }

    @Override
    public Flux<List<ServiceInstance>> get() {
        return Flux.just(Arrays
                .asList(new DefaultServiceInstance(serviceId + "1", serviceId, "localhost", 8765, false),
                        new DefaultServiceInstance(serviceId + "2", serviceId, "localhost", 8766, false),
                        new DefaultServiceInstance(serviceId + "3", serviceId, "localhost", 8767, false)));
    }
}
