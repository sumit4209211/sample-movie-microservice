package com.aniket.vanilajavaapicaller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class VanilaJavaApiCallerApplication {

    public static void main(String[] args) {
        SpringApplication.run(VanilaJavaApiCallerApplication.class, args);
    }

}
