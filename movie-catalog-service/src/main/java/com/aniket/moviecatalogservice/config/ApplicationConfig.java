package com.aniket.moviecatalogservice.config;

import brave.sampler.Sampler;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@EnableCircuitBreaker
public class ApplicationConfig {

    /**
     *
     * @return
     *
    TL;DR: @LoadBalanced is a marker annotation & @RibbonClient is used for configuration purposes.

     @LoadBalanced
     Used as a marker annotation indicating that the annotated RestTemplate should use a RibbonLoadBalancerClient for interacting with your service(s).

     In turn, this allows you to use "logical identifiers" for the URLs you pass to the RestTemplate. These logical identifiers are typically the name of a service. For example:

     restTemplate.getForObject("http://some-service-name/user/{id}", String.class, 1);
     where some-service-name is the logical identifier.

     @RibbonClient
     Used for configuring your Ribbon client(s).

     Is @RibbonClient required?

     No! If you're using Service Discovery and you're ok with all of the default Ribbon settings, you don't even need to use the @RibbonClient annotation.

     When should I use @RibbonClient?

     There are at least two cases where you need to use @RibbonClient

     You need to customize your Ribbon settings for a particular Ribbon client
     You're not using any service discovery
     Customizing your Ribbon settings:

     Define a @RibbonClient

     @RibbonClient(name = "some-service", configuration = SomeServiceConfig.class)
     name - set it to the same name of the service you're calling with Ribbon but need additional customizations for how Ribbon interacts with that service.
     configuration - set it to an @Configuration class with all of your customizations defined as @Beans. Make sure this class is not picked up by @ComponentScan otherwise it will override the defaults for ALL Ribbon clients.
     See the section "Customizing the RibbonClient` in the Spring Cloud Netflix documentation (link)

     Using Ribbon without Service Discovery

     If you're not using Service Discovery, the name field of the @RibbonClient annotation will be used to prefix your configuration in the application.properties as well as "logical identifier" in the URL you pass to RestTemplate.

     Define a @RibbonClient

     @RibbonClient(name = "myservice")
     then in your application.properties

     myservice.ribbon.eureka.enabled=false
     myservice.ribbon.listOfServers=http://localhost:5000, http://localhost:5001
     */

    @Bean
    @LoadBalanced // This can be used as alternative to Ribbon load balancer - Ribbon may go away in future
    //This was we can selectively initate a Ben in certain Environment
   // @Profile({"dev","prod"})
    public RestTemplate restTemplate()
    {
        //Normal Rest template
        //return new RestTemplate();

        //Using timeout for Service which is slow
        HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpComponentsClientHttpRequestFactory.setReadTimeout(1000); //If you are using Hystrix No need to add these
        return  new RestTemplate(httpComponentsClientHttpRequestFactory);

    }

    @Bean
    public Sampler defaultSampler() {
        return Sampler.ALWAYS_SAMPLE;
    }


    @Bean
    @LoadBalanced
    public WebClient.Builder webClientBuilder()
    {
        return WebClient.builder();
    }
}
