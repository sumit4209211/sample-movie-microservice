package com.aniket.moviecatalogservice.config;
import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties("prefix")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EnvironmentInformation {
private int portNumber;
private String technology;
private String serverType;
}
