package com.aniket.moviecatalogservice.service.impl;

import com.aniket.moviecatalogservice.config.EnvironmentInformation;
import com.aniket.moviecatalogservice.model.CatalogItem;
import com.aniket.moviecatalogservice.model.CatalogResponse;
import com.aniket.moviecatalogservice.model.MovieInfo;
import com.aniket.moviecatalogservice.model.MovieRating;
import com.aniket.moviecatalogservice.service.CatalogService;
import com.aniket.moviecatalogservice.utils.InfoServiceCallerUtils;
import com.aniket.moviecatalogservice.utils.RatingServiceCallerUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
@RefreshScope // To refresh the Values from Config Server
public class CatalogServiceImpl implements CatalogService {


    private final List<CatalogItem> catalogItems = new ArrayList<>();

    @PostConstruct
    public void DataLoad(){
        for (int i = 0; i < 20; i++) {
            catalogItems.add(new CatalogItem(i,null,null ,null));
        }
    }

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private Environment environment;

    @Autowired
    private  WebClient.Builder webClientBuilderl;

    @Autowired
    private RatingServiceCallerUtils ratingServiceCallerUtils;

    @Autowired
    private InfoServiceCallerUtils infoServiceCallerUtils;

    @Value("${list.values}")
    private List<String> numbers;

    @Value("#{${db.values}}")
    private Map<String,String> infos;

   @Value("${property-from-config}")

   //Some alternatives of using @Value
   //@Value("${property-from-config : Some Default Value}")
  // @Value("Some hardcoded value")
    private String propFromCloudConfig;

    @Autowired
    private EnvironmentInformation environmentInformation;


    @Override
    public CatalogResponse getCatalogResponse(String userId) {

        List<MovieRating> movieRatings = new ArrayList<>();
        catalogItems.forEach(item-> {

             MovieRating movieRating = ratingServiceCallerUtils.getMovieRating(restTemplate,item);
            //MovieRating movieRating = ServiceCallerUtils.getMovieRatingViaWebClient(webClientBuilderl,item);
            item.setMovieRating(movieRating.getMovieRating());
            try {
                MovieInfo movieInfo = infoServiceCallerUtils.getMovieInfo(restTemplate, item);
                item.setMovieName(movieInfo.getName());
                item.setMovieDescription(movieInfo.getDescription());
            }catch (Exception e){
                log.error("Timeout happened for ID : {}",item.getId());
            }

        });
        CatalogResponse catalogResponse = new CatalogResponse();
        catalogResponse.setUserId(userId);
        catalogResponse.setCatalogItems(catalogItems);
        catalogResponse.setInfos(infos);
        catalogResponse.setProperties(numbers);
        catalogResponse.setActiveProfiles(Arrays.asList(environment.getActiveProfiles()));
        catalogResponse.setPropFromCloudConfig(propFromCloudConfig);
        catalogResponse.setEnvironmentInformation(new EnvironmentInformation(environmentInformation.getPortNumber(),environmentInformation.getTechnology(),environmentInformation.getServerType()));
        return  catalogResponse;
    }
}
