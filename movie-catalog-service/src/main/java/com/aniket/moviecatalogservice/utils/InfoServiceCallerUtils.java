package com.aniket.moviecatalogservice.utils;

import com.aniket.moviecatalogservice.model.CatalogItem;
import com.aniket.moviecatalogservice.model.MovieInfo;
import com.aniket.moviecatalogservice.model.MovieRating;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@Component
@Slf4j
public class InfoServiceCallerUtils {

    @Value("${fallback.info.moviename : default Value In case Prop Do not Exist}")
private String fallbackMovieName;

    @Value("${fallback.info.moviedesc}")
    private String fallbackMovieDesc;


    @HystrixCommand(fallbackMethod = "getMovieInfoFallback",commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1500"),
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000")

    },threadPoolKey = "infoPool",threadPoolProperties = {
            @HystrixProperty(name = "coreSize", value = "20"),
            @HystrixProperty(name = "maxQueueSize", value = "10"),
    })
    public MovieInfo getMovieInfo(RestTemplate restTemplate, CatalogItem catalogItem){
        log.info("Calling Rest template for Movie Info");
        //Calling Directly
        //MovieInfo movieInfo = restTemplate.getForObject("http://localhost:8001/info/"+catalogItem.getId(),MovieInfo.class);
        //Calling via Eureka
        MovieInfo movieInfo = restTemplate.getForObject("http://MOVIE-INFO-SERVICE/info/"+catalogItem.getId(),MovieInfo.class);
        return movieInfo;
    }


    public MovieInfo getMovieInfoFallback(RestTemplate restTemplate, CatalogItem catalogItem){
        log.info("Fallback Method is being called for ID {}",catalogItem.getId());
        return new MovieInfo(catalogItem.getId(),fallbackMovieName,fallbackMovieDesc);
    }



    public MovieInfo getMovieInfoViaWebClient(WebClient.Builder builder, CatalogItem catalogItem){
        log.info("Calling Web Client for Movie Info");
        //Calling Directly
        //MovieInfo movieInfo =builder.build().get().uri("http://localhost:8001/info/"+catalogItem.getId()).retrieve().bodyToMono(MovieInfo.class).block();
        //Calling via Eureka
        MovieInfo movieInfo =builder.build().get().uri("http://MOVIE-INFO-SERVICE/info/"+catalogItem.getId()).retrieve().bodyToMono(MovieInfo.class).block();
        return movieInfo;
    }
}
