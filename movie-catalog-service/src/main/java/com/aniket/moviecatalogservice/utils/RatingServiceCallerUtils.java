package com.aniket.moviecatalogservice.utils;

import com.aniket.moviecatalogservice.model.CatalogItem;
import com.aniket.moviecatalogservice.model.MovieInfo;
import com.aniket.moviecatalogservice.model.MovieRating;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@Component
@Slf4j
public class RatingServiceCallerUtils {

    @Value("999.69")
    private String fallbackMovieRating;

    @HystrixCommand(fallbackMethod = "getMovieRatingFallback",commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000"),
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000")

    },threadPoolKey = "ratingPool",threadPoolProperties = {
            @HystrixProperty(name = "coreSize", value = "20"),
            @HystrixProperty(name = "maxQueueSize", value = "10"),
    })
    public MovieRating getMovieRating(RestTemplate restTemplate, CatalogItem catalogItem){
        log.info("Calling Rest template for Movie Rating");
        //Calling Directly
       // MovieRating movieRating = restTemplate.getForObject("http://localhost:7001/rating/"+catalogItem.getId(),MovieRating.class);
        //Calling via Eureka
        MovieRating movieRating = restTemplate.getForObject("http://MOVIE-RATING-SERVICE/rating/"+catalogItem.getId(),MovieRating.class);
        return movieRating;
    }

    public MovieRating getMovieRatingFallback(RestTemplate restTemplate, CatalogItem catalogItem){
        log.info("Calling Fallback Method for id {}",catalogItem.getId());
        return new MovieRating(catalogItem.getId(),/*new Double(fallbackMovieRating)*/Double.parseDouble(fallbackMovieRating));
    }



    public MovieRating getMovieRatingViaWebClient(WebClient.Builder builder, CatalogItem catalogItem){
        log.info("Calling Web Client for Movie Rating");
        //Calling Directly
        //MovieRating movieRating = builder.build().get().uri("http://localhost:7001/rating/"+catalogItem.getId()).retrieve().bodyToMono(MovieRating.class).block();
        //Calling via Eureka
        MovieRating movieRating = builder.build().get().uri("http://MOVIE-RATING-SERVICE/rating/"+catalogItem.getId()).retrieve().bodyToMono(MovieRating.class).block();
        return movieRating;
    }


}
