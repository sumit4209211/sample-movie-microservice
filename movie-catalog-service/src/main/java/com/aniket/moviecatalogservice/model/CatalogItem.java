package com.aniket.moviecatalogservice.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CatalogItem {
    int id;
    String movieName;
    String movieDescription;
    Double movieRating;
    
}
