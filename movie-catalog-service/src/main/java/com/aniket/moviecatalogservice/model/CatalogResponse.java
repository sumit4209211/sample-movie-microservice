package com.aniket.moviecatalogservice.model;

import com.aniket.moviecatalogservice.config.EnvironmentInformation;
import lombok.*;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CatalogResponse {

    private String userId;
    private List<CatalogItem> catalogItems;
    private List<String> properties;
    private Map<String,String> infos;
    private EnvironmentInformation environmentInformation;
    private List<String> activeProfiles;
    private String propFromCloudConfig;

}
